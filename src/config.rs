use lazy_static::lazy_static;
use serde_derive::{Deserialize};

use std::fs::File;
use std::io::prelude::*;
use toml;

#[derive(Deserialize)]
pub struct Config {
    pub connection: String,
    pub paper: String,
    pub username: String,
    pub password: String,
    pub bind: String,
    pub static_dir: String,
    pub tpl_dir: String
}

fn read_config() -> Config{
    let mut file = match File::open("Config.toml") {
        Ok(data) => data,
        Err(_) => match File::open("/etc/cartrige_manager/Config.toml") {
            Ok(data) => data,
            Err(err) => panic!("Cant ope a config ./Config.toml and /etc/cartrige_manager/Config.toml: {}", err)
        }
    };
    let mut string_config = String::new();
    file.read_to_string(&mut string_config).unwrap();
    let config: Config = match toml::from_str(string_config.as_str()) {
        Ok(data) => data,
        Err(err) => panic!("Ошибка чтения файла Config.toml: {}", err)
    };
    config
} 
lazy_static! {
    pub static ref CONFIG: Config = read_config();
}
