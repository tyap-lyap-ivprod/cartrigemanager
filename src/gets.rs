use actix_web::{web, Result, get};
use crate::auth::ses_in_group;
use crate::structs::*;
use crate::w_sql;
use crate::utils::{ cartrige_ctx,
                    model_ctx,
                    status_ctx,
                    dept_ctx,
                    page_ctx,
                    printer_ctx,
                    history_ctx,
                    username_ctx,
                    filter_emp,
                    redirect,
                    users_ctx,
                    user_ctx,
                    groups_ctx,
                    user_groups};
use actix_web::{error, Error, HttpResponse};
use actix_session::{Session};

use crate::auth::check_session;

#[get("/view_ctgr")]
async fn view_ctgr(
    tmpl: web::Data<tera::Tera>,
    info: web::Query<ViewCatInfo>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut client = w_sql::connect_db();
            let mut ctx = tera::Context::new();
            println!("content");
            ctx = cartrige_ctx(&mut client, ctx, info.id.parse::<i32>().unwrap());
            ctx = model_ctx(&mut client, ctx, false); 
            ctx = status_ctx(&mut client, ctx, false);
            ctx = dept_ctx(&mut client, ctx, false);
            ctx = printer_ctx(&mut client, ctx, false);
            ctx = history_ctx(&mut client, ctx, info.id.parse::<i32>().unwrap());
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("cartrige.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}
#[get("/add_cartrige")]
async fn add_cartrige_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
        let mut ctx = tera::Context::new();
        let mut client = w_sql::connect_db();
        ctx = model_ctx(&mut client, ctx, false); 
        ctx = status_ctx(&mut client, ctx, false);
        ctx = dept_ctx(&mut client, ctx, false);
        ctx = printer_ctx(&mut client, ctx, false);
            ctx = username_ctx(ctx, &mut client, &session);
        let s = tmpl.render("add_cartrige.html", &ctx)
            .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}

#[get("/add_model")]
async fn add_model_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = model_ctx(&mut client, ctx, true);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("add_model.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}

#[get("/add_dept")]
async fn add_dept_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = dept_ctx(&mut client, ctx, true);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("add_dept.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}
#[get("/add_status")]
async fn add_status_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = status_ctx(&mut client, ctx, true);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("add_status.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}
#[get("/add_printer")]
async fn add_printer_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = printer_ctx(&mut client, ctx, true);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("add_printer.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
                HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}

#[get("/view_page/{id_page}")]
async fn view_page(
    tmpl: web::Data<tera::Tera>,
    info: web::Path<Page>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = page_ctx(&mut client, ctx, info.id_page, &"id".to_string());
            ctx = model_ctx(&mut client, ctx, false); 
            ctx = status_ctx(&mut client, ctx, false);
            ctx = dept_ctx(&mut client, ctx, false);
            ctx = printer_ctx(&mut client, ctx, false);
            ctx = filter_emp(ctx);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("view_page.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}
#[get("/view_page/sort/{id_page}")]
async fn view_page_sort(
    tmpl: web::Data<tera::Tera>,
    info: web::Path<Page>,
    info1: web::Query<PageName>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = page_ctx(&mut client, ctx, info.id_page, &info1.sort);
            println!("{}", info1.sort);
            ctx = model_ctx(&mut client, ctx, false); 
            ctx = status_ctx(&mut client, ctx, false);
            ctx = dept_ctx(&mut client, ctx, false);
            ctx = printer_ctx(&mut client, ctx, false);
            ctx = filter_emp(ctx);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("view_page.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}
#[get("/view_page_filter/{id_page}")]
async fn view_page_filter(
    tmpl: web::Data<tera::Tera>,
    info: web::Path<Page>,
    info1: web::Query<Filter>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = model_ctx(&mut client, ctx, false); 
            ctx = status_ctx(&mut client, ctx, false);
            ctx = dept_ctx(&mut client, ctx, false);
            ctx = printer_ctx(&mut client, ctx, false);
            ctx = info1.filter_page(info.id_page, ctx);
            ctx = username_ctx(ctx, &mut client, &session);
            let s = tmpl.render("view_page.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)
        },
        false => {
            redirect("/login".to_string())
        }
    };
    Ok(resp)
}

#[get("/admin/edit_settings")]
async fn settings_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let mut client = w_sql::connect_db();
    let resp = match check_session(&session) {
        true => match ses_in_group(&mut client, &session, &1_i32) {
            true => {
                let mut ctx = tera::Context::new();
                ctx = Settings::ctx(ctx);
                let s = tmpl.render("settings.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError(":("))?;
                HttpResponse::Ok().content_type("text/html").body(s)
            },
            false => redirect("/login".to_string())
        }
        false => redirect("/login".to_string())
    };
    Ok(resp)
}

#[get("/login")]
async fn login_get(
    tmpl: web::Data<tera::Tera>,
    session: Session
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            redirect("/view_page/1".to_string())
        },
        false => {
            let mut ctx = tera::Context::new();
            ctx.insert("message", "lol");
            let s = tmpl.render("login.html", &ctx).unwrap();
            HttpResponse::Ok().content_type("text/html").body(s)
        } 
    };
    Ok(resp)
}
#[get("/error/{}")]
async fn error_page(
    info: web::Path<Errors>,
    tmpl: web::Data<tera::Tera>
) -> Result<HttpResponse, Error> {
    let mut ctx = tera::Context::new();
    ctx.insert("error", &info.error_string);
    let s = tmpl.render("error.html", &ctx).unwrap();
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/lk")]
async fn lk_page(
    session: Session,
    tmpl: web::Data<tera::Tera>
) -> Result<HttpResponse, Error> {
    let resp = match check_session(&session) {
        true => {
            let mut ctx = tera::Context::new();
            let mut client = w_sql::connect_db();
            ctx = username_ctx(ctx, &mut client, &session);
            ctx = users_ctx(ctx, &mut client);
            println!("{}", ses_in_group(&mut client, &session, &0_i32));
            let s = tmpl.render("lk.html", &ctx)
                .map_err(|_| error::ErrorInternalServerError(":("))?;
            HttpResponse::Ok().content_type("text/html").body(s)            
        },
        false => {                
            redirect("/login".to_string())
        }
    };
    Ok(resp)    
}
#[get("/mass_print")]
async fn mass_print_page(
    tmpl: web::Data<tera::Tera>
) -> Result<HttpResponse, Error>{
    let ctx = tera::Context::new();
    let s = tmpl.render("print_lbl.html", &ctx)
        .map_err(|_| error::ErrorInternalServerError("err"))?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}
#[get("/user/{user_id}")]
async fn user_page(
    session: Session,
    tmpl: web::Data<tera::Tera>,
    info: web::Path<i32>) -> Result<HttpResponse, Error> {
    let mut client = w_sql::connect_db();
    Ok(match check_session(&session) {
        true => match ses_in_group(&mut client, &session, &1_i32) {
            true => {
                let mut ctx = tera::Context::new();
                ctx = user_ctx(ctx, &mut client, &info);
                ctx = groups_ctx(ctx, &mut client);
                ctx = user_groups(ctx, &mut client, &info);
                let s = tmpl.render("user.html", &ctx)
                    .map_err(|_| error::ErrorInternalServerError("err"))?;
                HttpResponse::Ok().content_type("text/html").body(s)
            },
            false => redirect("/error/not_admin".to_string())
        },
        false => redirect("/error/not_admin".to_string())
    })
}

#[get("/")]
async fn root_page(
    session: Session
) -> Result<HttpResponse, Error>{
    let resp = match check_session(&session) {
        true => redirect("/view_page/1".to_string()),
        false => redirect("/login".to_string())
    };
    Ok(resp)
}