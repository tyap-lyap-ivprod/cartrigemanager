use actix_web::{web, Result, post};
use crate::structs::*;
use crate::utils::*;
use crate::w_sql;
use crate::auth;
use crate::auth::check_session;
use crate::auth::ses_in_group;
use actix_web::{Error, HttpResponse,HttpRequest};
use actix_session::Session;

#[post("/add_model_post")]
async fn add_model_post(form: web::Form<AddModel>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let name = &form.model_name;
            w_sql::add_model(&mut con, &name, &form.model_note);
            redirect_back(request)

        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/add_status_post")]
async fn add_status_post(form: web::Form<AddStatus>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let name = &form.status_name;
            w_sql::add_status(&mut con, &name, &form.status_note);
            redirect_back(request)

        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/add_dept_post")]
async fn add_dept_post(form: web::Form<AddDept>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let name = &form.dept_name;
            w_sql::add_dept(&mut con, &name, &form.dept_note);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_dept_post")]
async fn update_dept_post(form: web::Form<Dept>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let id   = &form.dept_id;
            let name = &form.dept_name;
            let note = &form.dept_note;
            let num = &form.dept_num;
            w_sql::update_dept(&mut con, &id, &name, &note, &num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_model_post")]
async fn update_model_post(form: web::Form<Model>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let id   = &form.model_id;
            let name = &form.model_name;
            let note = &form.model_note;
            let num = &form.model_num;
            w_sql::update_model(&mut con, &id, &name, &note, &num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}

#[post("/update_printer_post")]
async fn update_printer_post(form: web::Form<Printer>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let id   = &form.printer_id;
            let name = &form.printer_name;
            let glpi = &form.printer_glpi_id;
            let note = &form.printer_note;
            w_sql::update_printer(&mut con, &id, &name, &glpi, &note);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_status_post")]
async fn update_status_post(form: web::Form<Status>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let id   = &form.status_id;
            let name = &form.status_name;
            let note = &form.status_note;
            let num = &form.status_num;
            w_sql::update_status(&mut con, &id, &name, &note, &num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_dept_int")]
async fn update_dept_int_post(form: web::Form<UpdateInt>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::update_dept_int(&mut con, &form.id, &form.num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_model_int")]
async fn update_model_int_post(form: web::Form<UpdateInt>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::update_model_int(&mut con, &form.id, &form.num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_status_int")]
async fn update_status_int_post(form: web::Form<UpdateInt>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::update_status_int(&mut con, &form.id, &form.num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/add_cartrige_post")]
async fn add_cartrige_post(form: web::Form<CartrigeFromForm>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let session_id = match &session.get::<String>("session").unwrap() {
                Some(data) => format!("{}",data).to_string(),
                _ => "".to_string()
            };
            let user_raw = w_sql::get_userinfo_from_session(&mut con, &session_id);
            let user_id = user_raw[0].get::<_, i32>(0);
            w_sql::add_cartrige(&mut con, 
                                &form.model_id, 
                                &form.status_id,
                                &form.dept_id,
                                &form.cartrige_place,
                                &form.printer_id,
                                &form.cartrige_note,
                                &user_id);
        redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/add_cartrige_post_next")]
async fn add_cartrige_post_next(
    form: web::Form<CartrigeFromForm>,
    session: Session) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let session_id = match &session.get::<String>("session").unwrap() {
                Some(data) => format!("{}",data).to_string(),
                _ => "".to_string()
            };
            let user_raw = w_sql::get_userinfo_from_session(&mut con, &session_id);
            let user_id = user_raw[0].get::<_, i32>(0);
            let id = w_sql::add_cartrige(&mut con, 
                                &form.model_id, 
                                &form.status_id,
                                &form.dept_id,
                                &form.cartrige_place,
                                &form.printer_id,
                                &form.cartrige_note,
                                &user_id);
            redirect(format!("/view_ctgr?id={}", id))
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_cartrige")]
async fn update_cartrige_post(form: web::Form<CartrigeUpdate>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            let session_id = match &session.get::<String>("session").unwrap() {
                Some(data) => format!("{}",data).to_string(),
                _ => "".to_string()
            };
            let user_raw = w_sql::get_userinfo_from_session(&mut con, &session_id);
            let user_id = user_raw[0].get::<_, i32>(0);
            w_sql::update_cartrige(&mut con, 
                                    &form.cartrige_id, 
                                    &form.model_id, 
                                    &form.status_id, 
                                    &form.dept_id, 
                                    &form.cartrige_place, 
                                    &form.printer_id, 
                                    &form.cartrige_note,
                                    &user_id);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/add_printer_post")]
async fn add_printer_post(form: web::Form<AddPrinter>,
                        session: Session,
                          request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::add_printer(&mut con, 
                                &form.printer_name,
                                &form.printer_glpi_id,
                                &form.printer_note);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/update_printer_int")]
async fn update_printer_int_post(form: web::Form<UpdateInt>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::update_printer_int(&mut con, &form.id, &form.num);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}

///Delete (active, deactive)
#[post("/del_model_post")]
async fn del_model_post(form: web::Form<SwitchActive>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::model_active(&mut con, 
                                    &form.id_switched,
                                    &form.activation_status);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/del_status_post")]
async fn del_status_post(form: web::Form<SwitchActive>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::status_active(&mut con, 
                                    &form.id_switched,
                                    &form.activation_status);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/del_dept_post")]
async fn del_dept_post(form: web::Form<SwitchActive>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::dept_active(&mut con, 
                                    &form.id_switched,
                                    &form.activation_status);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/del_printer_post")]
async fn del_printer_post(form: web::Form<SwitchActive>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::printer_active(&mut con, 
                                    &form.id_switched,
                                    &form.activation_status);
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/del_cartrige_post")]
async fn del_cartrige_post(form: web::Form<SwitchActive>,
                        session: Session,
                              request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            let mut con = w_sql::connect_db();
            w_sql::cartrige_active(&mut con, 
                                    &form.id_switched,
                                    &form.activation_status);
                                    
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}

fn str_to_bool(string_val: String) -> bool{
    match string_val.as_str() {
        "true" => true,
        _ => false
    }
}

#[post("/update/settings")]
async fn update_settings_post(
    form:web::Form<SettingsString>,
    session: Session,
    request: HttpRequest) -> Result<HttpResponse, Error> {
    let mut client = w_sql::connect_db();
    Ok(
        match check_session(&session) {
            true => match ses_in_group(&mut client, &session, &1_i32) {
                true => {
                    let new_setings = Settings{
                        model_sort_ida: str_to_bool(form.model_sort_ida.clone()),
                        status_sort_ida: str_to_bool(form.status_sort_ida.clone()),
                        depts_sort_ida: str_to_bool(form.depts_sort_ida.clone()),
                        org_name: form.org_name.clone(),
                        admin_ex: str_to_bool(form.admin_ex.clone())
                    };
                    match w_sql::update_settings(&mut client, &new_setings) {
                        true => redirect_back(request),
                        false => redirect("/error/sql_error".to_string())
                    }
                    
                },
                false => redirect("/login".to_string())
            },
            false => redirect("/login".to_string())
        }
    )
}

#[post("/register")]
async fn register_post(  form: web::Form<LoginDate>,
                        session: Session,
                        request: HttpRequest) -> Result<HttpResponse, Error> {
    let response = match check_session(&session) {
        true => {
            auth::create_user(LoginDate{
                username: form.username.clone(),
                password: form.password.clone()
            });
            redirect_back(request)
        },
        false =>{
            redirect("/login".to_string())
        }
    };
    Ok(response)
}
#[post("/login")]
async fn login_post(
    form: web::Form<LoginDate>,
    session: Session
    )-> Result<HttpResponse, Error> {
    println!("Try to login");
    let response = match auth::check_pass(LoginDate{
        username: form.username.clone(),
        password: form.password.clone()
    }){
        true => {
            let ses: String = auth::gen_session(form.username.clone());
            session.set("session", ses).unwrap();
            redirect("/".to_string())
        },
        false => {
            println!("ERROR! NOT LOGON");
            redirect("/error/not_login".to_string())     
        }
    };
    Ok(response)
}
#[post("/logout")]
async fn logout_post(session: Session) -> Result<HttpResponse, Error> {
    session.clear();
    Ok(redirect("/login".to_string()))
}
#[post("/update_password")]
async fn update_password_post(
    session: Session,
    form: web::Form<LoginDate>,
    request: HttpRequest) -> Result<HttpResponse, Error>{
    let mut client = w_sql::connect_db();
    let response = match check_session(&session){
        true => match ses_in_group(&mut client, &session, &1_i32){
            true => {
                auth::update_password(&form.username, &form.password);
                redirect_back(request)
            },
            false => redirect("/error/not_admin".to_string())
        },
        false => redirect("/login".to_string())
    };
    Ok(response)
}
#[post("/set_admin")]
async fn set_admin_post(
    session: Session,
    form: web::Form<UserId>,
    request: HttpRequest) -> Result<HttpResponse, Error> {
    Ok(
        match check_session(&session){
            true => {
                let mut client = w_sql::connect_db();
                match ses_in_group(&mut client, &session, &1_i32){
                    true => {
                        match w_sql::active_user_table(&mut client, &form.user_id, &1_i32, &true){
                            true => redirect_back(request),
                            _ => redirect("/error/wsql".to_string())
                        }
                    },
                    false => redirect("/error/not_admin".to_string())
                }
            },
            false => {
                redirect("/error/not_registr".to_string())
            }
        }
    )
}
#[post("/set_nadmin")]
async fn set_nadmin_post(
    session: Session,
    form: web::Form<UserId>,
    request: HttpRequest) -> Result<HttpResponse, Error> {
    Ok(
        match check_session(&session){
            true => {
                let mut client = w_sql::connect_db();
                match ses_in_group(&mut client, &session, &1_i32){
                    true => {
                        match w_sql::active_user_table(&mut client, &form.user_id, &1_i32, &false){
                            true => redirect_back(request),
                            _ => redirect("/error/wsql".to_string())
                        }
                    },
                    false => redirect("/error/not_admin".to_string())
                }
            },
            false => {
                redirect("/error/not_registr".to_string())
            }
        }
    )
}
#[post("/hide_user")]
async fn hide_user_post(
    session: Session,
    form: web::Form<UserId>,
    request: HttpRequest) -> Result<HttpResponse, Error> {
    Ok(
        match check_session(&session){
            true => {
                let mut client = w_sql::connect_db();
                match ses_in_group(&mut client, &session, &1_i32){
                    true => {
                        match w_sql::user_active(&mut client, &form.user_id, &false){
                            Ok(_data) => redirect_back(request),
                            _ => redirect("/error/wsql".to_string())
                        }
                    },
                    false => redirect("/error/not_admin".to_string())
                }
            },
            false => {
                redirect("/error/not_registr".to_string())
            }
        }
    )
}
#[post("/add_group")]
async fn add_group_user(
    session: Session,
    form: web::Form<AddGroup>,
    request: HttpRequest) -> Result<HttpResponse, Error> {
        let mut client = w_sql::connect_db();
        Ok( match check_session(&session){
            true => match ses_in_group(&mut client, &session, &1_i32){
                true => match w_sql::add_group(&mut client, &form.group_name) {
                    0 => redirect("/error/not".to_string()),
                    _ => redirect_back(request)
                },
                false => redirect("/error/not_admin".to_string())
            },
            false => redirect("/error/not_registr".to_string())
        })
}
#[post("/update_self_pass")]
async fn update_self_pass(
    session: Session,
    form: web::Form<Password>,
    request: HttpRequest) -> Result<HttpResponse, Error> {
    let mut client = w_sql::connect_db();
    let response = match check_session(&session){
        true => {
            let session_id = match &session.get::<String>("session").unwrap() {
                Some(data) => format!("{}",data).to_string(),
                _ => "".to_string()
            };
            let username_raw = w_sql::get_userinfo_from_session(&mut client, &session_id);
            match username_raw.len(){
                0 => redirect("/error".to_string()),
                _ =>{
                    let username = username_raw[0].get::<_, String>(1);
                    auth::update_password(&username, &form.password);
                    redirect_back(request)
                }
            }
        },
        false => redirect("/error".to_string())
    };
    Ok(response)
}
#[post("/user_group_activate")]
async fn user_group_activate(
    session: Session,
    form: web::Form<UserGroupStatus>,
    request: HttpRequest
) -> Result<HttpResponse, Error> {
    let mut client = w_sql::connect_db();
    Ok( match check_session(&session){
        true => match ses_in_group(&mut client, &session, &1_i32){
            true => {
                match w_sql::is_in_group(&mut client, &form.user_id, &form.group_id) {
                    true  => {
                        match w_sql::active_user_table(&mut client, &form.user_id, &form.group_id, &match form.status{
                            0 => false,
                            _ => true
                        }) {
                            true =>{
                                println!("Update");
                                redirect_back(request)
                            },
                            false => redirect("/error/fail".to_string())
                        }
                    },
                    false => {
                        match form.status {
                            0 => {
                                println!("Not found");
                                redirect_back(request)
                            },
                            _ => {
                                match w_sql::add_to_group(&mut client, &form.user_id, &form.group_id) {
                                    true => {
                                        println!("Add new");
                                        redirect_back(request)
                                    },
                                    false => redirect("/error/fail".to_string())
                                }
                            }
                        }
                    }
                }
            },
            false => redirect("/error/not_admin".to_string())
        },
        false => redirect("/error/not_registr".to_string())
    })
}