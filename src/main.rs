use actix_web::{middleware, App, HttpServer};
use tera::Tera;
use actix_files as fs_ac;
use rand::Rng; // 0.8

use std::env;

use actix_session::CookieSession;
use cartrigemanager::w_sql;
use cartrigemanager::posts::*;
use cartrigemanager::gets::*;
use cartrigemanager::auth;
use cartrigemanager::config::CONFIG;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    let mut client = w_sql::connect_db();
    let args: Vec<String> = env::args().collect();
    println!("{:?}", args);
    if args.len() > 1 {
        if args[1] == "init" {
            println!("Init");
            w_sql::init_db(&mut client);
            auth::init();
        }
        else if args[1] == "drop" {
            println!("Drop table");
            w_sql::kill_db(&mut client);
        }
        else if args[1] == "reinit" {
            println!("Reinit table");
            w_sql::kill_db(&mut client);
            w_sql::init_db(&mut client);
            auth::init();
        }
        else if args[1] == "password_reset" {
            println!("UPDATE password");
            auth::password_reset();
        }
    }
    HttpServer::new(|| {
//      let tera = Tera::new(concat!(env!("CARGO_MANIFEST_DIR"), "/templates/**/*")).unwrap();
        let tera = Tera::new(&CONFIG.tpl_dir.as_str()).unwrap();
        let _private_key = rand::thread_rng().gen::<[u8; 32]>();
        App::new()
            .data(tera)
            .wrap(middleware::Logger::default()) // enable logger
            .wrap(CookieSession::signed(&[0; 32]).secure(false))
            .service(root_page)
            .service(view_ctgr)
            .service(view_page)
            .service(add_model_post)
            .service(add_model_get)
            .service(add_status_post)
            .service(add_status_get)
            .service(add_dept_post)
            .service(add_dept_get)
            .service(add_cartrige_get)
            .service(add_cartrige_post)
            .service(add_cartrige_post_next)
            .service(add_printer_get)
            .service(add_printer_post)

            .service(update_cartrige_post)
            .service(update_dept_post)
            .service(update_status_post)
            .service(update_model_post)
            .service(update_printer_post)

            .service(update_model_int_post)
            .service(update_status_int_post)
            .service(update_dept_int_post)
            .service(update_printer_int_post)
            .service(del_model_post)
            .service(del_status_post)
            .service(del_dept_post)
            .service(del_printer_post)
            .service(del_cartrige_post)
            .service(view_page_sort)
            .service(view_page_filter)

            .service(login_post)
            .service(register_post)
            .service(logout_post)
            .service(login_get)
            .service(lk_page)
            .service(update_password_post)
            .service(set_admin_post)
            .service(set_nadmin_post)
            .service(hide_user_post)
            .service(add_group_user)
            .service(update_self_pass)
            .service(user_group_activate)

            .service(add_cartrige_post_next)
            .service(update_settings_post)
            .service(settings_get)
            .service(mass_print_page)
            .service(user_page)
            .service(fs_ac::Files::new("/static", CONFIG.static_dir.as_str()).show_files_listing())
    })
    .bind(CONFIG.bind.clone())?
    
    .run()
    .await
}
