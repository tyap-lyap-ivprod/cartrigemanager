use actix_session::Session;
use actix_web::{HttpResponse,HttpRequest};
use tera::Context;
use postgres::Client;
use crate::auth::get_username;
use crate::w_sql;
use crate::structs::*;
use crate::auth::{ses_in_group};
use time::PrimitiveDateTime;

pub(crate) fn redirect_back(request: HttpRequest)-> HttpResponse {
    let a = request.headers().get("REFERER").unwrap().to_str().unwrap();
    println!("a = {}", a);
    HttpResponse::Found()
            .header("Location", a).finish()
}

pub(crate) fn redirect(a: String)-> HttpResponse {
    HttpResponse::Found()
            .header("Location", a).finish()
}

pub(crate) fn status_ctx(mut client: &mut Client, mut ctx: Context,
                        active: bool) -> Context {
    let raw_statuses = w_sql::select_status(&mut client);
    let mut statuses = Vec::new();
    for i in raw_statuses{
        let printer_active = i.get::<_, bool>(4);
        if active | printer_active {
            statuses.push(Status{
                status_id:   i.get::<_, i32>(0),
                status_name: i.get::<_, String>(1),
                status_note: i.get::<_, String>(2),
                status_num : i.get::<_, i32>(3),
                status_active: printer_active,
            });
        }
    }
    ctx.insert("statuses", &statuses);
    ctx
}
pub(crate) fn model_ctx(mut client: &mut Client, mut ctx: Context,
                        active: bool) -> Context {
    let raw_models = w_sql::select_model(&mut client);   
    let mut models = Vec::new();
    for i in raw_models{
        let model_active = i.get::<_, bool>(4);
        if active | model_active {
            models.push(Model{
                model_id:    i.get::<_, i32>(0),
                model_name:  i.get::<_, String>(1),
                model_note:  i.get::<_, String>(2),
                model_num:   i.get::<_, i32>(3),
                model_active: model_active
            });
        }
    }
    ctx.insert("models", &models);
    ctx
}
pub(crate) fn dept_ctx( mut client: &mut Client, 
                        mut ctx: Context,
                        active: bool) -> Context {
    let raw_depts = w_sql::select_dept(&mut client);   
    let mut depts = Vec::new();
    for i in raw_depts{
        let dept_active = i.get::<_, bool>(4);
        if active | dept_active {
            depts.push(Dept{
                dept_id:     i.get::<_, i32>(0),
                dept_name:   i.get::<_, String>(1),
                dept_note:   i.get::<_, String>(2),
                dept_num:    i.get::<_, i32>(3),
                dept_active: dept_active
            });
        }
    }
    ctx.insert("depts", &depts);
    ctx
}
pub(crate) fn printer_ctx(mut client: &mut Client, mut ctx: Context, active: bool) -> Context {
    let raw_printers = w_sql::select_printer(&mut client);   
    let mut printers = Vec::new();
    for i in raw_printers{
        let printer_active = i.get::<_, bool>(5);
        if active | printer_active {
            printers.push(Printer{
                printer_id:   i.get::<_, i32>(0),
                printer_glpi_id: i.get::<_, String>(1),
                printer_name: i.get::<_, String>(2),
                printer_note: i.get::<_, String>(3),
                printer_num: i.get::<_, i32>(4),
                printer_active: printer_active
            });
        }
    }
    ctx.insert("printers", &printers);
    ctx
}
pub(crate) fn history_ctx(mut client: &mut Client, mut ctx: Context, cartrige_id: i32) -> Context {
    let raw_history = w_sql::cartrige_history(&mut client, &cartrige_id);   
    let mut history = Vec::new();
    for i in raw_history{
        let date = i.get::<_, PrimitiveDateTime>(6).format("%F-%R");
        history.push(History{
            history_id: i.get::<_, i32>(0),
            history_model: i.get::<_, String>(1),
            history_status: i.get::<_, String>(2),
            history_dept: i.get::<_, String>(3),
            history_place: i.get::<_, String>(4),
            history_printer: i.get::<_, String>(5),
            history_date: date,
            history_user: i.get::<_, String>(7),
            history_note: i.get::<_, String>(8),
        });
    }
    ctx.insert("history", &history);
    ctx
}
pub(crate) fn page_ctx(mut client: &mut Client, mut ctx: Context, page_id: i32, sort: &String) -> Context {
    let raw_cartriges = w_sql::select_page_catrige(&mut client, &page_id, &sort);
    let mut cartriges = Vec::new();
    for cartrige in raw_cartriges{
        cartriges.push(Cartrige{
            cartrige_id :        cartrige.get::<_, i32>(0),
            model_id :           cartrige.get::<_, String>(1),
            status_id :          cartrige.get::<_, String>(2),
            dept_id:             cartrige.get::<_, String>(3),
            cartrige_place :     cartrige.get::<_, String>(4),
            printer_id :         cartrige.get::<_, String>(5),
            cartrige_date :      cartrige.get::<_, PrimitiveDateTime>(6).format("%F %R"),
            user1 :              cartrige.get::<_, String>(7),
            cartrige_note :      cartrige.get::<_, String>(8),
            active:              cartrige.get::<_, bool>(9)
        })
    }
    ctx.insert("cartriges", &cartriges);
    ctx.insert("page", &page_id);
    ctx.insert("sort", &sort);
    ctx.insert("cart_n", &w_sql::select_len_page_cartrige_filter(&mut client, &Filter::emp()));
    ctx
}
pub(crate) fn cartrige_ctx(mut client: &mut Client, mut ctx: Context, cartrige_id: i32) -> Context {
    let array_from_sql = w_sql::select_one_catrige(&mut client, &cartrige_id);
    let from_sql = &array_from_sql[0];
    let cartrige = Cartrige{
        cartrige_id     : from_sql.get::<_, i32>(0),
        model_id        : from_sql.get::<_, String>(1),
        status_id       : from_sql.get::<_, String>(2),
        dept_id         : from_sql.get::<_, String>(3),
        cartrige_place  : from_sql.get::<_, String>(4),
        printer_id      : from_sql.get::<_, String>(5),
        cartrige_date   : from_sql.get::<_, PrimitiveDateTime>(6).format("%R %F"),
        user1           : from_sql.get::<_, String>(7),
        cartrige_note   : from_sql.get::<_, String>(8),
        active          : from_sql.get::<_, bool>(9)

    };
    ctx.insert("cartrige", &cartrige);
    ctx
}

impl Filter{
    pub fn emp() -> Filter{
        Filter{
            status_id:0, 
            model_id:0, 
            dept_id:0,
            sort: "".to_string() 
        }
    }
    pub fn filter_page(&self, page_i: i32,  mut ctx: Context) -> Context{
        let mut con = w_sql::connect_db();
        let from_sql = w_sql::select_page_catrige_filter(&mut con, &page_i, &self.sort, self);
        let mut cartriges = Vec::new();
        for cartrige in from_sql{
            cartriges.push(Cartrige{
                cartrige_id :        cartrige.get::<_, i32>(0),
                model_id :           cartrige.get::<_, String>(1),
                status_id :          cartrige.get::<_, String>(2),
                dept_id:             cartrige.get::<_, String>(3),
                cartrige_place :     cartrige.get::<_, String>(4),
                printer_id :         cartrige.get::<_, String>(5),
                cartrige_date :      cartrige.get::<_, PrimitiveDateTime>(6).format("%F %R"),
                user1 :              cartrige.get::<_, String>(7),
                cartrige_note :      cartrige.get::<_, String>(8),
                active:              cartrige.get::<_, bool>(9)
            })
        };
        ctx.insert("cart_n", &w_sql::select_len_page_cartrige_filter(&mut con, self));
        ctx.insert("cart_len", &cartriges.len());
        ctx.insert("cartriges", &cartriges);
        ctx.insert("page", &page_i);
        ctx.insert("sort", &self.sort);
        ctx.insert("filters", &self);
        ctx
    }
}
pub fn filter_emp(mut ctx: Context) -> Context{
    ctx.insert("filters", &Filter::emp());
    ctx
}

impl Settings {
    pub fn ctx(mut ctx: Context) -> Context{
        let mut con = w_sql::connect_db();
        let settings = w_sql::settings(&mut con);
        ctx.insert("settings", &settings);
        ctx
    }
    pub fn ctx_emp(mut ctx: Context) -> Context{
        ctx.insert("settings", &Settings::emp());
        ctx
    }
}

pub fn admin_ctx(mut ctx: Context,
    mut client: &mut Client,
    session: &Session) -> Context{
    ctx.insert("admin", &ses_in_group(&mut client, session, &1_i32));
    ctx
}

pub fn username_ctx(mut ctx: Context,
                    mut client: &mut Client,
                    session: &Session) -> Context {
    let user = get_username(&session, &mut client).unwrap();
    ctx.insert("username", &user.username);
    let user_admin = w_sql::is_in_group(&mut client, &user.user_id, &1_i32);
    ctx.insert("admin", &user_admin);
    ctx
}

pub fn users_ctx(mut ctx: Context,
                 mut client: &mut Client) -> Context {
    let raw_users = w_sql::users(&mut client);
    let mut users = Vec::new();
    for raw_user in raw_users{
        let user_id = raw_user.get::<_, i32>(0);
        let user_name = raw_user.get::<_, String>(1);
        let user_admin =  w_sql::is_in_group(&mut client, &user_id, &1_i32);
        
        users.push(UserIdName{
            id: user_id,
            name: user_name,
            admin: user_admin
        })
    };
    ctx.insert("users", &users);
    ctx
}
pub fn user_ctx(
    mut ctx: Context,
    mut client: &mut Client,
    user_id: &i32) -> Context {
    let raw_user = w_sql::get_user(&mut client, &user_id);
    ctx.insert("user", &match raw_user {
        Ok(data) => match data.len() {
            1 => {
                UserIdName{
                    id: data[0].get::<_, i32>(0),
                    name: data[0].get::<_, String>(1),
                    admin:  w_sql::is_in_group(&mut client, &user_id, &1_i32)
                }
            },
            _ => UserIdName::emp()
        },
        Err(_) => UserIdName::emp()
    });
    ctx
}

pub fn groups_ctx(
    mut ctx: Context,
    mut client: &mut Client
) -> Context {
    let raw_groups = w_sql::groups(&mut client);
    ctx.insert("groups", &match raw_groups {
        Ok(data) => match data.len() {
            0 => vec![Group::emp()],
            _ => {
                let mut all_groups = Vec::new();
                for group in data {
                    all_groups.push(Group{
                        id: group.get::<_, i32>(0),
                        name: group.get::<_, String>(1)
                    })
                }
                all_groups
            }
        },
        Err(_) => vec![Group::emp()]
    });
    ctx
}

pub fn user_groups(
    mut ctx: Context,
    mut client: &mut Client,
    user_id: &i32
) -> Context {
    let raw_user_groups = w_sql::user_groups(&mut client, &user_id);
    ctx.insert("user_groups", &match raw_user_groups{
        Ok(data) => match data.len() {
            0 => vec![IdGroup::emp()],
            _ => {
                let mut all_groups = Vec::new();
                for group in data {
                    all_groups.push(IdGroup{
                        id: group.get::<_, i32>(0),
                        name: group.get::<_, String>(1),
                        group_id: group.get::<_, i32>(2)
                    })
                }
                all_groups
            }
        },
        Err(_) => vec![IdGroup::emp()]
    });
    ctx
}