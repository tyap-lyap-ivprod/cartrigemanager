use serde_derive::{Serialize, Deserialize};
//Models for get data from request
#[derive(Deserialize, Serialize)]
pub struct Model{
    pub model_id: i32,
    pub model_name: String,
    pub model_note: String,
    pub model_num: i32,
    pub model_active: bool, 
}
#[derive(Deserialize)]
pub struct AddModel{
    pub model_name: String,
    pub model_note: String,
}

#[derive(Deserialize, Serialize)]
pub struct Status{
    pub status_id: i32,
    pub status_name: String,
    pub status_note: String,
    pub status_num: i32,
    pub status_active: bool,
}
#[derive(Deserialize)]
pub struct AddStatus{
    pub status_name: String,
    pub status_note: String,
}

#[derive(Deserialize, Serialize)]
pub struct Dept{
    pub dept_id: i32,
    pub dept_name: String,
    pub dept_note: String,
    pub dept_num: i32,
    pub dept_active: bool,
}
#[derive(Deserialize)]
pub struct AddDept{
    pub dept_name: String,
    pub dept_note: String,
}
#[derive(Deserialize, Serialize)]
pub struct Filter{
    pub status_id: i32,
    pub model_id: i32,
    pub dept_id: i32,
    pub sort: String
}

#[derive(Deserialize, Serialize)]
pub struct Printer{
    pub printer_id: i32,
    pub printer_glpi_id: String,
    pub printer_name: String,
    pub printer_note: String,
    pub printer_num: i32,
    pub printer_active: bool
}
#[derive(Deserialize)]
pub struct AddPrinter{
    pub printer_glpi_id: String,
    pub printer_name: String,
    pub printer_note: String,
}

#[derive(Deserialize, Serialize)]
pub struct UpdateInt{
    pub id: i32,
    pub num: i32
}
#[derive(Deserialize)]
pub struct ViewCatInfo{
    pub id: String,
}
#[derive(Serialize)]
pub struct Cartrige{
    pub cartrige_id: i32,
    pub model_id:  String,
    pub status_id: String,
    pub dept_id:   String,
    pub cartrige_place:  String,
    pub printer_id:String,
    pub cartrige_date:   String,
    pub cartrige_note:   String,
    pub user1: String,
    pub active: bool
}
#[derive(Deserialize)]
pub struct CartrigeFromForm{
    pub model_id:  i32,
    pub status_id: i32,
    pub dept_id:   i32,
    pub cartrige_place:  String,
    pub printer_id :i32,
    pub cartrige_note:   String
}
#[derive(Deserialize)]
pub struct CartrigeUpdate{
    pub cartrige_id:    i32,
    pub model_id:       i32,
    pub status_id:      i32,
    pub dept_id:        i32,
    pub cartrige_place: String,
    pub printer_id:     i32,
    pub cartrige_note:  String,
}
#[derive(Serialize)]
pub struct History{
    pub history_id: i32,
    pub history_model: String,
    pub history_status: String,
    pub history_dept: String,
    pub history_place: String,
    pub history_printer: String,
    pub history_date: String,
    pub history_note: String,
    pub history_user: String
}
#[derive(Deserialize)]
pub struct Page{
    pub id_page: i32
}
#[derive(Deserialize)]
pub struct PageName{
    pub sort: String
}
#[derive(Deserialize)]
pub struct SwitchActive{
    pub id_switched: i32,
    pub activation_status: bool
}
#[derive(Serialize)]
pub struct Settings{
    pub model_sort_ida: bool,
    pub depts_sort_ida: bool,
    pub status_sort_ida: bool,
    pub org_name: String,
    pub admin_ex: bool,
}
impl Settings {
    pub fn emp() -> Settings {
        Settings{
            model_sort_ida: false,
            depts_sort_ida: false,
            status_sort_ida: false,
            org_name: "".to_string(),
            admin_ex: false
        }
    }
}
#[derive(Deserialize)]
pub struct SettingsString{
    pub model_sort_ida: String,
    pub depts_sort_ida: String,
    pub status_sort_ida: String,
    pub org_name: String,
    pub admin_ex: String,
}
#[derive(Deserialize)]
pub struct LoginDate{
    pub username: String,
    pub password: String,
}
#[derive(Deserialize)]
pub struct UserId{
    pub user_id: i32
}
#[derive(Serialize)]
pub struct UserIdName{
    pub id: i32,
    pub name: String,
    pub admin: bool
}
impl UserIdName {
    pub fn emp() -> UserIdName{
        UserIdName{
            id: 0,
            name: "".to_string(),
            admin: false
        }
    }
}
#[derive(Serialize)]
pub struct Group{
    pub id: i32,
    pub name: String 
}
impl Group{
    pub fn emp() -> Group{
        Group{
            id: 0_i32,
            name: String::new()
        }
    }
}

#[derive(Deserialize)]
pub struct UserGroup{
    pub user_id: i32,
    pub group_id: i32
}
#[derive(Deserialize)]
pub struct Errors{
    pub error_string: String
}
#[derive(Deserialize)]
pub struct AddGroup{
    pub group_name: String
}

#[derive(Deserialize)]
pub struct Password{
    pub password: String
}
#[derive(Deserialize)]
pub struct UserGroupStatus{
    pub user_id: i32,
    pub group_id: i32,
    pub status: i32
}
#[derive(Serialize)]
pub struct IdGroup{
    pub id: i32,
    pub group_id: i32,
    pub name: String
}
impl IdGroup{
    pub fn emp() -> IdGroup{
        IdGroup{
            id: 0_i32,
            group_id: 0_i32,
            name: String::new()
        }
    }
}