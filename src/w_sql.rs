use postgres::row::Row;
use postgres::{Client, NoTls};
use crate::config::CONFIG;
use crate::structs::{self, Settings};
pub fn connect_db() -> Client {
    match Client::connect(&CONFIG.connection, NoTls) {
        Ok(data) => data,
        Err(err) => panic!("Не удаётся подключиться к базе данных: {}", err)
    }
}
use std::convert::TryFrom;
/// Создание базы
pub fn init_db(client: &mut Client) {
    client.batch_execute("
    CREATE TABLE models(
            id      SERIAL PRIMARY KEY,
            name    TEXT NOT NULL UNIQUE,
            notes   TEXT DEFAULT ' ',
            inta    INTEGER DEFAULT 1,
            active  BOOLEAN DEFAULT TRUE
        );
        INSERT INTO models (name)
                    VALUES ('');

        CREATE TABLE statuses(
            id      SERIAL PRIMARY KEY,
            name    TEXT NOT NULL UNIQUE,
            notes   TEXT DEFAULT ' ',
            inta    INTEGER DEFAULT 1,
            active  BOOLEAN DEFAULT TRUE
        );
        INSERT INTO statuses (name)
                    VALUES ('');

        CREATE TABLE depts(
            id      SERIAL PRIMARY KEY,
            name    TEXT NOT NULL UNIQUE,
            notes   TEXT DEFAULT ' ',
            inta    INTEGER DEFAULT 1,
            active  BOOLEAN DEFAULT TRUE
        );
        INSERT INTO depts (name)
                    VALUES ('');
        CREATE TABLE printers(
            id      SERIAL PRIMARY KEY,
            name    TEXT NOT NULL,
            glpi_id TEXT DEFAULT ' ',
            notes   TEXT DEFAULT ' ',
            inta    INTEGER DEFAULT 1,
            active  BOOLEAN DEFAULT TRUE
        );
        INSERT INTO printers (name, glpi_id)
                    VALUES ('', '');

        CREATE TABLE users(
            id SERIAL PRIMARY KEY,
            login TEXT NOT NULL UNIQUE,
            password TEXT NOT NULL,
            salt TEXT NOT NULL,
            admin   BOOLEAN DEFAULT FALSE,
            inta    INTEGER DEFAULT 1,
            active  BOOLEAN DEFAULT TRUE
        );
        CREATE TABLE cartriges(
            id      SERIAL PRIMARY KEY, 
            model   INTEGER NOT NULL,
            statu_s INTEGER NOT NULL,
            dept    INTEGER DEFAULT 1,
            place   TEXT DEFAULT ' ',
            printer INTEGER DEFAULT 1,
            date1   TIMESTAMP DEFAULT now(),
            notes   TEXT DEFAULT ' ',
            user1   INTEGER DEFAULT 1, 
            FOREIGN KEY (model)    REFERENCES models, 
            FOREIGN KEY (statu_s)   REFERENCES statuses,
            FOREIGN KEY (dept)     REFERENCES depts,  
            FOREIGN KEY (printer)  REFERENCES printers,
            FOREIGN KEY (user1)  REFERENCES users,
            active  BOOLEAN DEFAULT TRUE
        );
        CREATE TABLE history(
            id      SERIAL PRIMARY KEY, 
            model   TEXT DEFAULT '',
            statu_s TEXT DEFAULT '',
            dept    TEXT DEFAULT '',
            place   TEXT DEFAULT '',
            printer TEXT DEFAULT '',
            date1   TIMESTAMP,
            login   TEXT DEFAULT '',
            notes    TEXT DEFAULT '',
            idctr   INTEGER,
            FOREIGN KEY (idctr) REFERENCES cartriges,
            active  BOOLEAN DEFAULT TRUE
        );
        CREATE TABLE session(
            id SERIAL PRIMARY KEY,
            user_id INTEGER NOT NULL,
            ses_id TEXT,
            active BOOL,
            FOREIGN KEY (user_id) REFERENCES users(id)
        );
        CREATE TABLE groups(
            id SERIAL PRIMARY KEY,
            group_name TEXT,
            description TEXT
        );
        INSERT INTO groups(group_name, description) VALUES ('admin', 'Can adding users');
        CREATE TABLE user_group(
            id SERIAL PRIMARY KEY,
            group_id    INTEGER,
            user_id     INTEGER,
            active      BOOL,
            FOREIGN KEY (user_id) REFERENCES users(id),
            FOREIGN KEY (group_id) REFERENCES groups(id)
        );
        CREATE TABLE project_settings(
            id SERIAL PRIMARY KEY,
            model_sort_ida BOOL,
            depts_sort_ida BOOL,
            status_sort_ida BOOL,
            org_name TEXT,
            admin_ex BOOL
        );
        INSERT into project_settings
            (model_sort_ida, depts_sort_ida, status_sort_ida, org_name, admin_ex) 
            VALUES(true, true, false, 'Irk OIKB', false);").unwrap();
}

pub fn kill_db(client: &mut Client) {
    client.batch_execute("DROP TABLE history;
                          DROP TABLE cartriges;
                          DROP TABLE models;
                          DROP TABLE printers;
                          DROP TABLE statuses;
                          DROP TABLE depts;
                          DROP TABLE session;
                          DROP TABLE user_group;
                          DROP TABLE groups;
                          DROP TABLE users;
                          DROP TABLE project_settings;
                          ").unwrap();
}

pub fn select_one_catrige(client: &mut Client,
                          id: &i32) -> Vec<Row>{
    client.query("SELECT cartriges.id, 
                         models.name, 
                         statuses.name, 
                         depts.name, 
                         cartriges.place, 
                         printers.name ,
                         cartriges.date1,
                         users.login,
                         cartriges.notes,
                         cartriges.active
                FROM cartriges
                INNER JOIN models ON models.id = cartriges.model
                INNER JOIN statuses ON statuses.id = cartriges.statu_s
                INNER JOIN depts ON depts.id = cartriges.dept
                INNER JOIN printers ON printers.id = cartriges.printer
                INNER JOIN users ON users.id = cartriges.user1
                WHERE (cartriges.id = $1);", &[id]).unwrap()
}
pub fn cartrige_active(client: &mut Client,
                          id: &i32,
                          active: &bool) -> Vec<Row>{
    client.query("UPDATE cartriges SET active = $2 
                    WHERE cartriges.id = $1", &[id, active]).unwrap()
}
pub fn add_cartrige(client: &mut Client, 
                    model:    &i32,
                    status:   &i32,
                    depts:    &i32,
                    place:    &String,
                    printers: &i32,
                    note:     &String,
                    login:    &i32) -> i32{
    let a = client.query("INSERT INTO cartriges 
                       (model, statu_s, dept,  place, date1, printer, notes, user1)
                VALUES($1,    $2,       $3,    $4,    now(), $5,       $6,    $7)
RETURNING id", &[model, status,  depts, place,        printers, note, login]).unwrap();
    let id = a[0].get::<_, i32>(0);
    client.query("INSERT INTO history 
                 (idctr,       model,       statu_s,        dept,       place, date1, printer, notes, login)
        SELECT    cartriges.id, models.name, statuses.name, depts.name, cartriges.place, cartriges.date1, printers.name, cartriges.notes, users.login
            FROM cartriges
            INNER JOIN models ON models.id = cartriges.model
            INNER JOIN statuses ON statuses.id = cartriges.statu_s
            INNER JOIN depts ON depts.id = cartriges.dept
            INNER JOIN printers ON printers.id = cartriges.printer
            INNER JOIN users ON users.id = cartriges.user1
        WHERE cartriges.id = $1;", &[&id]).unwrap();
    id
}
pub fn update_cartrige( client: &mut Client,
                        id:       &i32,
                        model:    &i32,
                        status:   &i32,
                        depts:    &i32,
                        place:    &String,
                        printers: &i32,
                        note:     &String,
                        login:    &i32) -> Vec<Row>{
    client.query("UPDATE cartriges SET
                  (model, statu_s, dept, place, date1, printer, notes, user1) =
                  ($1,    $2,      $3,   $4,    now(), $5,      $6  , $8) WHERE id=$7", 
           &[model, status, depts, place,       printers, note, id, login]).unwrap();
    client.query("INSERT INTO history 
                 (idctr,       model,       statu_s,        dept,       place, date1, printer,      notes, login)
        SELECT    cartriges.id, models.name, statuses.name, depts.name, cartriges.place, cartriges.date1, printers.name, cartriges.notes, users.login           FROM cartriges
            INNER JOIN models ON models.id = cartriges.model
            INNER JOIN statuses ON statuses.id = cartriges.statu_s
            INNER JOIN depts ON depts.id = cartriges.dept
            INNER JOIN printers ON printers.id = cartriges.printer
            INNER JOIN users ON users.id = cartriges.user1
        WHERE cartriges.id = $1;", &[id]).unwrap()
}
pub fn cartrige_len(client: &mut Client) -> i32{
    let a = client.query("SELECT COUNT(*) FROM cartriges WHERE active=true", &[]).unwrap();
    a.len() as i32
}
fn sort_page(sort: &String) -> String{
    format!("ORDER BY {}", match sort.as_str() {
        "id"        => "cartriges.id",
        "id_d"      => "cartriges.id DESC",
        "status"    => "statuses.name",
        "status_d"  => "statuses.name DESC",
        "model"     => "models.name",
        "model_d"   => "models.name DESC",
        "dept"      => "depts.name",
        "dept_d"    => "depts.name DESC",
        "place"     => "cartriges.place", 
        "place_d"   => "cartriges.place DESC", 
        "printer"   => "printers.name",
        "printer_d" => "printers.name DESC",
        "date"      => "cartriges.date1",
        "date_d"    => "cartriges.date1 DESC",
        "note"      => "cartriges.notes",
        "note_d"    => "cartriges.notes DESC",
        _           => "cartriges.id"
    })
}
pub fn select_page_catrige(client: &mut Client, 
                           page: &i32, 
                           sort_val: &String) ->Vec<Row> {
    let sorta = sort_page(sort_val);
    let querrry = format!("SELECT cartriges.id, 
                         models.name, 
                         statuses.name, 
                         depts.name, 
                         cartriges.place, 
                         printers.name ,
                         cartriges.date1,
                         users.login,
                         cartriges.notes,
                         cartriges.active
                FROM cartriges
                INNER JOIN models ON models.id = cartriges.model
                INNER JOIN statuses ON statuses.id = cartriges.statu_s
                INNER JOIN depts ON depts.id = cartriges.dept
                INNER JOIN printers ON printers.id = cartriges.printer
                INNER JOIN users ON users.id = cartriges.user1
                WHERE cartriges.active = true
                {}
                OFFSET ($1 - 1) * 100 LIMIT 100;",sorta);
    let aaas = querrry.as_str();
    client.query(aaas, &[page]).unwrap()
}

fn filter_page(name: &str, filter_id: &i32) -> String{
    if filter_id > &1 {
        format!("AND {} = {}", name, filter_id)
    }
    else {
        String::from("")
    }
}

pub fn select_len_page_cartrige_filter(client: &mut Client, 
                                        filter_st: &structs::Filter) -> i32 {
    let filter_status = filter_page("cartriges.statu_s", &filter_st.status_id);
    let filter_model = filter_page("cartriges.model", &filter_st.model_id);
    let filter_dept = filter_page("cartriges.dept", &filter_st.dept_id);
    let querrry = format!("SELECT count(*) 
    FROM cartriges
    WHERE cartriges.active = true
    {}
    {}
    {};", filter_status, filter_model, filter_dept);
    let querrry = querrry.as_str();
    let na = client.query(querrry, &[]).unwrap();
    match i32::try_from(na[0].get::<_, i64>(0)){
        Ok(ler) => ler,
        _             => 0_i32
    }
}

pub fn select_page_catrige_filter(  client: &mut Client, 
                                    page: &i32, 
                                    sort_val: &String, 
                                    filter_st: &structs::Filter) ->Vec<Row> {
    let sorta = sort_page(sort_val);
    let filter_status = filter_page("cartriges.statu_s", &filter_st.status_id);
    let filter_model = filter_page("cartriges.model", &filter_st.model_id);
    let filter_dept = filter_page("cartriges.dept", &filter_st.dept_id);
    let querrry = format!("SELECT cartriges.id, 
                         models.name, 
                         statuses.name, 
                         depts.name, 
                         cartriges.place, 
                         printers.name ,
                         cartriges.date1,
                         users.login,
                         cartriges.notes,
                         cartriges.active
                FROM cartriges
                INNER JOIN models ON models.id = cartriges.model
                INNER JOIN statuses ON statuses.id = cartriges.statu_s
                INNER JOIN depts ON depts.id = cartriges.dept
                INNER JOIN printers ON printers.id = cartriges.printer
                INNER JOIN users ON users.id = cartriges.user1
                WHERE cartriges.active = true
                {}
                {}
                {}
                {}
                OFFSET ($1 - 1) * 100 LIMIT 100;", filter_status, filter_dept, filter_model, sorta);
    let aaas = querrry.as_str();
    client.query(aaas, &[page]).unwrap()
}

pub fn add_model(client: &mut Client, name: &String, note: &String) -> Vec<Row> {
    client.query("INSERT INTO models (name, notes)
                  VALUES ($1, $2)
                  RETURNING id;", &[name, note]).unwrap()
}
pub fn select_model(client: &mut Client) -> Vec<Row> {
    match settings(client).model_sort_ida{
        true => client.query("SELECT id, name, notes, inta, active FROM models ORDER BY name;", &[]).unwrap(),
        false => client.query("SELECT id, name, notes, inta, active FROM models ORDER BY inta;", &[]).unwrap()
    }
}
pub fn update_model( client: &mut Client, 
                    id:   &i32,
                    name: &String,
                    note: &String,
                    num:  &i32) -> Vec<Row> {
    client.query("UPDATE models SET (name, notes, inta) =
                                   ($1,   $2, $4)
                  WHERE id=$3;",&[name, note, id, num]).unwrap()
}
pub fn update_model_int( client: &mut Client, 
                    id:   &i32,
                    inta: &i32) -> Vec<Row> {
    client.query("UPDATE models SET inta = $2
                  WHERE id=$1;",&[id, inta]).unwrap()
}
pub fn model_active( client: &mut Client, 
                    id:   &i32,
                    active: &bool) -> Vec<Row> {
    client.query("UPDATE models SET active = $2
                  WHERE id=$1;",&[id, active]).unwrap()
}

pub fn add_status(client: &mut Client, name: &String, note: &String) -> Vec<Row> {
    client.query("INSERT INTO statuses (name, notes)
                  VALUES ($1, $2)
                  RETURNING id;", &[name, note]).unwrap()
}
pub fn select_status(client: &mut Client) -> Vec<Row> {
    match settings(client).status_sort_ida{
        true =>  client.query("SELECT id, name, notes, inta, active FROM statuses ORDER BY name;", &[]).unwrap(),
        false => client.query("SELECT id, name, notes, inta, active FROM statuses ORDER BY inta;", &[]).unwrap()
    }
}
pub fn update_status( client: &mut Client, 
                    id:   &i32,
                    name: &String,
                    note: &String,
                    num: &i32) -> Vec<Row> {
    client.query("UPDATE statuses SET (name, notes, inta) =
                                   ($1,   $2, $4)
                  WHERE id=$3;",&[name, note, id, num]).unwrap()
}
pub fn update_status_int( client: &mut Client, 
                    id:   &i32,
                    inta: &i32) -> Vec<Row> {
    client.query("UPDATE statuses SET inta = $2
                  WHERE id=$1;",&[id, inta]).unwrap()
}
pub fn status_active( client: &mut Client, 
                    id:   &i32,
                    active: &bool) -> Vec<Row> {
    client.query("UPDATE statuses SET active=$2
                  WHERE id=$1;",&[id, active]).unwrap()
}

pub fn add_dept(client: &mut Client, name: &String, note: &String) -> Vec<Row> {
    client.query("INSERT INTO depts (name, notes)
                  VALUES ($1, $2)
                  RETURNING id;", &[name, note]).unwrap()
}
pub fn select_dept(client: &mut Client) -> Vec<Row> {
    match settings(client).depts_sort_ida{
        true => client.query("SELECT id, name, notes, inta, active FROM depts ORDER BY name;", &[]).unwrap(),
        false => client.query("SELECT id, name, notes, inta, active FROM depts ORDER BY inta;", &[]).unwrap()
    }
}
pub fn update_dept( client: &mut Client, 
                    id:   &i32,
                    name: &String,
                    note: &String,
                    num:  &i32) -> Vec<Row> {
    client.query("UPDATE depts SET (name, notes, inta) =
                                   ($1,   $2, $4)
                  WHERE id=$3;",&[name, note, id, num]).unwrap()
}
pub fn update_dept_int( client: &mut Client, 
                    id:   &i32,
                    inta: &i32) -> Vec<Row> {
    client.query("UPDATE depts SET inta = $2
                  WHERE id=$1;",&[id, inta]).unwrap()
}
pub fn dept_active( client: &mut Client, 
                    id:   &i32,
                    active: &bool) -> Vec<Row> {
    client.query("UPDATE depts SET active=$2
                  WHERE id=$1;",&[id, active]).unwrap()
}

pub fn add_printer(client: &mut Client, name: &String, 
                                        glpi_id: &String,
                                        note: &String) -> Vec<Row> {
    client.query("INSERT INTO printers (name, glpi_id, notes)
                  VALUES ($1, $2, $3)
                  RETURNING id;", &[name,glpi_id,note]).unwrap()
}
pub fn select_printer(client: &mut Client) -> Vec<Row> {
    client.query("SELECT id, name, glpi_id, notes, inta, active FROM printers ORDER BY inta;", &[]).unwrap()
}
pub fn update_printer( client: &mut Client, 
                    id: &i32,
                    name: &String,
                    glpi_id: &String,
                    note: &String) -> Vec<Row> {
    client.query("UPDATE printers SET (name, glpi_id, notes) =
                      ($1, $2, $3)
                      WHERE id=$4;",&[name, glpi_id, note, id]).unwrap()
}
pub fn update_printer_int( client: &mut Client, 
                    id:   &i32,
                    inta: &i32) -> Vec<Row> {
    client.query("UPDATE printers SET inta = $2
                  WHERE id=$1;",&[id, inta]).unwrap()
}
pub fn printer_active( client: &mut Client, 
                    id:   &i32,
                    active: &bool) -> Vec<Row> {
    client.query("UPDATE printers SET active=$2
                  WHERE id=$1;",&[id, active]).unwrap()
}

pub fn cartrige_history(client: &mut Client,
                        cartrige_id: &i32) -> Vec<Row>{
    client.query("SELECT id, model, statu_s, dept, 
                         place, printer, date1, 
                         login, notes
                 FROM history
                 WHERE idctr=$1
                 ORDER BY history.id DESC;", &[cartrige_id]).unwrap()
}
pub fn get_userdate(client: &mut Client, username: &String) -> Vec<Row> {
    client.query("SELECT id, login, password, salt FROM users WHERE login = $1", &[username]).unwrap()
}
pub fn get_user(client: &mut Client, user_id: &i32) -> Result<Vec<Row>, postgres::Error> {
    client.query("SELECT id, login, admin FROM users WHERE id = $1", &[user_id])
}
pub fn add_userdate(client: &mut Client, 
                    username: &String, 
                    passhash: &String, 
                    salt: &String
                    ) -> Vec<Row>{
    let a = client.query("INSERT INTO users (login, password, salt)
                  VALUES ($1, $2, $3) RETURNING id;", &[username, passhash, salt]).unwrap();
    let id = a[0].get::<_, i32>(0);
    client.query("INSERT INTO user_group (user_id, group_id, active) VALUES($1, $2, $3);", &[&id, &1_i32, &false]).unwrap()
}
pub fn add_session(client: &mut Client,
                   user_id: &i32,
                   session_id: &String) -> Vec<Row>{
    client.query("INSERT INTO session (user_id, ses_id, active)
                  VALUES ($1, $2, true)", &[user_id, session_id]).unwrap()
}
pub fn get_all_active_user(client: &mut Client) -> Vec<Row>{
    client.query("SELECT login FROM users", &[]).unwrap()
}
pub fn get_all_ses_for_admin_users(client: &mut Client) -> Vec<Row>{
   client.query("SELECT user_id, ses_id
                 FROM session WHERE active = true", &[]).unwrap()
}
pub fn get_ses_from_userdate(client: &mut Client, user: &String) -> Vec<Row> {
    client.query("SELECT session.ses_id 
                  FROM session
                  INNER JOIN users ON users.id = session.user_id AND users.login = $1", &[user]).unwrap()
}

pub fn password_update(client: &mut Client, username: &String, new_hash: &String, salt: &String){
    let id_cl = client.query("UPDATE users SET (password, salt) = ($1, $3) WHERE login = $2 RETURNING id", &[new_hash, username, salt]).unwrap();
    for i in id_cl{
        client.query("UPDATE session SET active = false WHERE id = $1", &[&i.get::<_, i32>(0)]).unwrap();
    }
}

pub fn settings(client: &mut Client) -> Settings{
    let buf_c = client.query("SELECT model_sort_ida, depts_sort_ida, status_sort_ida, org_name, admin_ex FROM project_settings;", &[]).unwrap();
    Settings{
        model_sort_ida: buf_c[0].get::<_, bool>(0),
        depts_sort_ida:       buf_c[0].get::<_, bool>(1),
        status_sort_ida:       buf_c[0].get::<_, bool>(2),
        org_name:       buf_c[0].get::<_, String>(3),
        admin_ex:       buf_c[0].get::<_, bool>(4),
    }
}

pub fn update_settings(client: &mut Client, p_settings: &Settings) -> bool {
    match client.query("UPDATE project_settings SET (model_sort_ida, depts_sort_ida, status_sort_ida, org_name, admin_ex) = ($1, $2, $3, $4, $5);", 
    &[&p_settings.model_sort_ida, &p_settings.depts_sort_ida, &p_settings.status_sort_ida, &p_settings.org_name, &p_settings.admin_ex]) {
        Ok(_) => true,
        Err(err) => {
            println!("{}",err);
            false
        }
    }
}

pub fn user_active(
    client: &mut Client,
    user_id: &i32,
    active: &bool
    ) -> Result<bool, postgres::Error>{
    match client.query("UPDATE users SET active = $2 WHERE id = $1 RETURNING active", &[user_id, active]) {
        Ok(data) => match data.len() {
            1 => Ok(data[0].get::<_, bool>(0)),
            _ => Ok(false)
        },
        Err(error) => Err(error)
    }
}
pub fn get_userinfo_from_session(client: &mut Client, user: &String) -> Vec<Row> {
    client.query("SELECT users.id, users.login, users.admin FROM session
                        INNER JOIN users ON users.id = user_id
                        WHERE ses_id =$1;", &[user]).unwrap()
}

pub fn user_id_from_session(client: &mut Client, user: &String) -> Result<i32, postgres::Error> {
    match client.query("SELECT user_id FROM session WHERE ses_id = $1", &[user]) {
        Ok(data) => match data.len() {
               1 => Ok(data[0].get::<_, i32>(0)),
               _ => Ok(0_i32)
            },
        Err(err)  => Err(err)
    }
}

pub fn users(client: &mut Client) -> Vec<Row> {
    client.query("SELECT id, login, admin FROM users WHERE active", &[]).unwrap()
}
pub fn set_admin(client: &mut Client, id_adminate: &i32, adminate: &bool) -> Result<Vec<Row>, postgres::Error> {
    client.query("UPDATE users SET admin = $2 WHERE id=$1 RETURNING id", &[id_adminate, adminate])
}
pub fn is_admin(client: &mut Client, id_adminate: &i32) -> Result<Vec<Row>, postgres::Error> {
    client.query("SELECT id FROM users WHERE id = $1 AND admin = true;", &[id_adminate])
}
pub fn is_in_group(client: &mut Client, id_user: &i32, id_group: &i32) -> bool {
    let a1 = match client.query("SELECT active FROM user_group WHERE user_id = $1 AND group_id = $2;", &[id_user, id_group]) {
        Ok(data) => {
            println!("lolaaaaa {}", &data.len());
            match data.len() {
                0 => false,
                _ => data[0].get::<_, bool>(0)
            }
        },
        Err(a) => {
            println!("{:?}", a);
            false
        }
    };
    println!(">>>{}",&a1);
    a1
}
pub fn add_to_group(client: &mut Client, id_user: &i32, id_group: &i32) -> bool {
    match client.query("INSERT INTO user_group (user_id, group_id) VALUES($1, $2) RETURNING id;", &[id_user, id_group]) {
        Ok(data) => match data.len() {
            0 => false,
            _ => true,
        },
        Err(_) => false
    }
}
pub fn add_group(client: &mut Client, group_name: &String) -> i32 {
    /*match client.query("INSERT INTO groups (group_name) VALUES($1) RETURNIN id;", &[id_userшш, id_group]) {
        Ok(data) => match data.len() {
            1 => data[0].get::<_, i32>(0),
            _ => 0_i32
        },
        Err(_) => 0_i32
    }*/
    let a1 = client.query("INSERT INTO groups (group_name) VALUES($1) RETURNING id;", &[group_name]).unwrap();
    a1[0].get::<_, i32>(0)
}
pub fn groups(client: &mut Client) -> Result<Vec<Row>, postgres::Error> {
    client.query("SELECT id, group_name, description FROM groups;",&[])
}
pub fn user_groups(
    client: &mut Client, 
    user_id: &i32
) -> Result<Vec<Row>, postgres::Error>{
    client.query("SELECT user_group.id, groups.group_name, user_group.group_id FROM user_group
    INNER JOIN groups ON groups.id = user_group.group_id
    WHERE user_id = $1 and active = true;", &[user_id])
}

pub fn active_user_table(
    client: &mut Client, 
    user: &i32, 
    group: &i32, 
    active: &bool) -> bool {
        match client.query("UPDATE user_group SET active=$1 WHERE user_id = $2 AND group_id = $3;", &[active, user, group]) {
            Ok(_) => {
                println!("UPDATE user_group");
                true
            },
            Err(_) => match client.query("INSERT INTO user_group (user_id, group_id, active) VALUES($1, $2, $3)", &[user, group, active]){
                Ok(_) => {
                    println!("INSERT user_group");
                    true
                },
                Err(_) => false
            }
        }
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_select_printer(){
        let mut client = connect_db();
        select_printer(&mut client);
    }
    #[test]
    fn test_select_dept(){
        let mut client = connect_db();
        select_dept(&mut client);
    }
    #[test]
    fn test_select_status(){
        let mut client = connect_db();
        select_status(&mut client);
    }
    #[test]
    fn test_select_page(){
        let mut client = connect_db();
        select_page_catrige_filter(&mut client, &1, &"id".to_string(), &structs::Filter::emp());
        select_page_catrige_filter(&mut client, &1, &"id".to_string(), &structs::Filter::emp());
        select_page_catrige_filter(&mut client, &1, &"id".to_string(), &structs::Filter::emp());
    }
    #[test]
    fn test_one_select_page(){
        let mut client = connect_db();
        select_page_catrige_filter(&mut client, &1, &"id".to_string(), &structs::Filter::emp());
    }
}
