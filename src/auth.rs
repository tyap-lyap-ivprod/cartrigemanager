use rand::{distributions::Alphanumeric, Rng}; // 0.8
use argon2::{self, Config};
use actix_session::{Session};
use postgres::Client;

use crate::structs::LoginDate;
use crate::w_sql;
use crate::config::CONFIG;

pub fn check_pass(login_date: LoginDate) -> bool{
    let config = Config::default();
    let mut con = w_sql::connect_db();
    let userdate_list = w_sql::get_userdate(&mut con, &login_date.username);
    let userdate_raw = match userdate_list.len() {
        0 => None,
        _ => Some(&userdate_list[0])
    };
    match userdate_raw {
        Some(userdate) => {
            let base_logindate = LoginDate {
                username: userdate.get::<_, String>(1),
                password: userdate.get::<_, String>(2)
            };

            let mut result1 = false;
            if base_logindate.username == login_date.username {
                let salt = userdate.get::<_, String>(3);
                let hash1 = argon2::hash_encoded(login_date.password.as_bytes(), 
                                                format!("{}{}", salt, CONFIG.paper).as_bytes(), 
                                                &config).unwrap();
                println!("\n>>>now_salt = {}", salt);
                println!("\n>>>new_cash={}\n>>>old_cash={}", hash1, base_logindate.password);
                result1 = hash1 == base_logindate.password;
            }
            result1
        } 
        None => false
    }
}
pub fn gen_salt() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(32)
        .map(char::from)
        .collect() 
}

pub fn create_user(login_date: LoginDate) {
    let mut con = w_sql::connect_db();
    let config = Config::default();
    let salt = gen_salt();
    let hash = argon2::hash_encoded(login_date.password.as_bytes(), 
                                    format!("{}{}", salt, CONFIG.paper).as_bytes(), 
                                    &config).unwrap();
    w_sql::add_userdate(&mut con, 
                        &login_date.username, 
                        &hash, 
                        &salt); 
    println!("{:?}", hash);
}

pub fn init() {
    create_user(LoginDate{
        username: CONFIG.username.clone(),
        password: CONFIG.password.clone()
    })
}

pub fn password_reset() {
    update_password(&CONFIG.username.clone(), &CONFIG.password.clone());
}

pub fn gen_session(username: String) -> String{
    let mut con = w_sql::connect_db();
    let salt = gen_salt();
    let userdate_list = w_sql::get_userdate(&mut con, &username);
    match userdate_list.len() {
        0 => "anonim".to_string(),
        _ => {
            let user_id = userdate_list[0].get::<_, i32>(0);
            println!(">>> login_id = {}", user_id);
            w_sql::add_session(&mut con, &user_id, &salt);
            salt
        }
    }
}
pub fn check_session(session: &Session) -> bool {
    let session_string = session.get::<String>("session").unwrap().unwrap_or("None".to_string());
    let mut con = w_sql::connect_db();
    let usersessions = w_sql::get_all_ses_for_admin_users(&mut con);
    let mut list_a = Vec::new();
    for session_el in usersessions {
        list_a.push(session_el.get::<_, String>(1));
    }
    list_a.contains(&session_string)
}
pub fn update_password(username: &String, password: &String){
    let mut con = w_sql::connect_db();
    let config = Config::default();
    let salt = gen_salt();
    let hash = argon2::hash_encoded(password.as_bytes(), 
        format!("{}{}", salt, CONFIG.paper).as_bytes(), 
        &config).unwrap();
                w_sql::password_update(&mut con, &username, &hash, &salt);
}
pub struct UserInfo{
    pub user_id: i32,
    pub username: String,
    pub admin:  bool
}
impl UserInfo {
    pub fn emp() -> UserInfo{
        UserInfo{
            user_id: 0,
            username: "".to_string(),
            admin: false
        }
    }
}
pub fn get_username(
    session: &Session,
    mut client: &mut Client) -> Result<UserInfo, String> {
    let session_id = match &session.get::<String>("session").unwrap() {
        Some(data) => format!("{}",data).to_string(),
        _ => "".to_string()
    };
    let raw_l = w_sql::get_userinfo_from_session(&mut client, &session_id);
    match raw_l.len() {
        1 => Ok(UserInfo{
            user_id:  raw_l[0].get::<_, i32>(0),
            username: raw_l[0].get::<_, String>(1),
            admin:    raw_l[0].get::<_, bool>(2)}
        ),
        _ => Err("Error".to_string())
    }
}
pub fn is_admin(
    con: &mut Client, 
    session: &Session) -> bool{
        let session_id = session.get::<String>("session").unwrap();
        match session_id {
            Some(session_cooked) => {
                let number = w_sql::user_id_from_session(con, &session_cooked);
                match number {
                    Ok(num) => match num {
                        0 => false,
                        _ => match w_sql::is_admin(con, &num) {
                            Ok(data) => data.len() > 0,
                            Err(_) => false
                        }
                    },
                    Err(_) => false
                }
            },
            _ => false
        }
}
pub fn ses_in_group(
    mut client: &mut Client,
    session: &Session,
    group: &i32) -> bool {
        match get_username(&session, &mut client) {
            Ok(user) => {
                println!("\n\n>>>{}", &user.user_id);
                w_sql::is_in_group(&mut client, &user.user_id, &group)
            },
            Err(_) => false
        }
}